import React from 'react'
import { StackNavigator, addNavigationHelpers } from 'react-navigation'
import { connect } from 'react-redux'

import FormLogin from '../formLogin'

export const RootNavigator = StackNavigator({
  FormLogin: {
    screen: FormLogin,
    navigationOptions: {
      headerTitle: 'US News Agregator'
    }
  }
})

const Navigator = ({ dispatch, nav }) => (
  <RootNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
);

const mapStateToProps = (state) => ({
  nav: state.nav
})

export default connect(null, null)(RootNavigator)
