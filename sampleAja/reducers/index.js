import { combineReducers } from 'redux'
import Sources from './sourcesReducer'

const rootReducer = combineReducers({
  Sources,
})

export default rootReducer
