import React from 'react';
import { StyleSheet, Text, View, TextInput, Button, Picker } from 'react-native';

export default class FormLogin extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      language: 'Java'
    }
  }
  loginPressed () {
    this.props.navigation.navigate('FormRegister', {username: this.state.username, password: this.state.password})
  }
  render() {
    return (
      <View style={{width: 250, height: 350, backgroundColor: '#FFF', justifyContent: 'center', alignItems: 'center', borderRadius: 50}}>
        <View style={styles.header}>
          <Text style={{fontSize: 24, textAlign: 'center', color: 'white'}}> Login to Your Account </Text>
        </View>
        <Picker
          selectedValue={this.state.language}
          style={{ height: 50, width: 100 }}
          onValueChange={(itemValue, itemIndex) => this.setState({language: itemValue})}>
          <Picker.Item label="Java" value="java" />
          <Picker.Item label="JavaScript" value="js" />
        </Picker>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#4A4A4A',
    alignItems: 'center',
    justifyContent: 'center',
  },
  header: {
    justifyContent: 'center',
    backgroundColor: 'red',
    marginTop: 20,
    marginRight: 20,
    marginLeft: 20,
    marginBottom: 20
  },
  body: {
    justifyContent: 'center',
    flexDirection: 'column'
  },
  footer: {
    alignItems: 'center',
    width: 250
  }
});
